<?php

namespace AppBundle\Controller;

use AppBundle\Form\Filter\OferteApartamentFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Ofertaapartament controller.
 *
 * @Route("admin/ofertaapartament")
 */
class OfertaApartamentController extends Controller
{
    private $em;

    public function __construct()
    {
        $this->em = $this->getDoctrine()->getManager();
    }


    /**
     * Lists all ofertaApartament entities.
     *
     * @Route("/", name="ofertaapartament_index")
     * @Method({"GET","POST"})
     */
    public function indexAction(Request $request)
    {
        $this->unsetFilterSession($request);

        $filterBuilder = $this->em->getRepository('AppBundle:OfertaApartament')->getQueryBuilderOferteAgentie($this->getUser());

        $form = $this->get('form.factory')->create(OferteApartamentFilterType::class);

        $session = $request->getSession();
        if ($request->request->has($form->getName())) {
            $form_data = $request->request->get($form->getName());
            $session->set('filter_form_ova', $form_data);
            $form->submit($form_data);

        } else {
            if (!empty($session->get('filter_form_ova'))) {
                $form->submit($session->get('filter_form_ova'));
            }
        }

        $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $filterBuilder);

        $query = $filterBuilder->getQuery();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            30
        );

        return $this->render('ofertaapartament/index.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView(),
        ));
    }


    /**
     * Unset filter session
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function unsetFilterSession(Request $request)
    {
        $session = $request->getSession();
        if ($request->query->get('op') == "init_filter") {
            $session->remove('filter_form_ova');
            return $this->redirectToRoute('ofertaapartament_index');
        }
    }
}
