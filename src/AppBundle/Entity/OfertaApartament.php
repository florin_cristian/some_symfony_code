<?php

namespace AppBundle\Entity;

use AppBundle\Form\Type\NomenclatorType;
use AppBundle\Model\AbstractModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * OfertaApartament
 *
 * @ORM\Table(name="oferta_apartament")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfertaApartamentRepository")
 * @ORM\HasLifecycleCallbacks
 */
class OfertaApartament extends AbstractModel
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MatchVA", mappedBy="ofertaApartament")
     */
    private $matchVA;

    /**
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="oferteApartament", cascade={"persist"})
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    private $contact;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $mapLat;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $mapLng;

    /**
     * @ORM\OneToOne(targetEntity="Gallery", mappedBy="ofertaApartament", cascade={"persist"})
     */
    private $gallery;

    /**
     * @var int
     *
     * @ORM\Column(name="camere", type="integer")
     * @Assert\NotBlank()
     */
    private $camere;

    /**
     * @var string
     *
     * @ORM\Column(name="descriere_emotionala", type="text", nullable=true)
     */
    private $descriereEmotionala;

    /**
     * @var string
     *
     * @ORM\Column(name="descriere_personala", type="text", nullable=true)
     */
    private $descrierePersonala;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresa1;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $adresa2;

    /**
     * @var int
     * @ORM\Column(name="pret", type="integer")
     * @Assert\NotBlank()
     */
    private $pret;

    /**
     * @var int
     * @ORM\Column(name="comision_procent", type="integer",nullable=true)
     */
    private $comisionProcent;

    /**
     * @var int
     * @ORM\Column(name="comision_suma", type="integer",nullable=true)
     */
    private $comisionSuma;

    /**
     * @var string
     * @ORM\Column(name="certificat_energetic", type="string", length=1, nullable=true)
     */
    private $certificatEnergetic;

    /**
     * @var string
     * @ORM\Column(name="reper", type="string",nullable=true)
     */
    private $reper;

    /**
     * @var int
     *
     * @ORM\Column(name="suprafata_utila", type="smallint",nullable=true)
     * * @Assert\Regex(
     *     pattern="/\d/",
     *     message="Acest camp contine numai cifre"
     * )
     * @Assert\Regex(
     *     pattern="/\D/",
     *      match=false,
     *     message="Acest camp contine numai cifre"
     * )
     */
    private $suprafataUtila;

    /**
     * @var int
     *
     * @ORM\Column(name="suprafata_construita", type="smallint",nullable=true)
     * * @Assert\Regex(
     *     pattern="/\d/",
     *     message="Acest camp contine numai cifre"
     * )
     * @Assert\Regex(
     *     pattern="/\D/",
     *      match=false,
     *     message="Acest camp contine numai cifre"
     * )
     */
    private $suprafataConstruita;

    /**
     * @var int
     *
     * @ORM\Column(name="suprafata_balcon_terasa", type="smallint",nullable=true)
     * * @Assert\Regex(
     *     pattern="/\d/",
     *     message="Acest camp contine numai cifre"
     * )
     * @Assert\Regex(
     *     pattern="/\D/",
     *      match=false,
     *     message="Acest camp contine numai cifre"
     * )
     */
    private $suprafataBalconTerasa;


    /**
     * @ORM\ManyToOne(targetEntity="NomenclatorImobile")
     * @ORM\JoinColumn(name="tipcompartimentare_id", referencedColumnName="id")
     */
    protected $tipCompartimentare;



    /**
     * @ORM\ManyToOne(targetEntity="Agentie")
     * @ORM\JoinColumn(name="agentie_id", referencedColumnName="id")
     */
    protected $agentie;



    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="oferteApartament")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $agent;

    /**
     * @ORM\ManyToOne(targetEntity="NomenclatorZone", inversedBy="oferteApartamente")
     * @ORM\JoinColumn(name="zona_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $zona;


    /**
     * @var bool
     *
     * @ORM\Column(name="temp", type="boolean", nullable=true)
     */
    private $temp;

    /**
     * @var bool
     *
     * @ORM\Column(name="cadastru", type="boolean", nullable=true)
     */
    private $cadastru;

    /**
     * @var bool
     *
     * @ORM\Column(name="intabulare", type="boolean", nullable=true)
     */
    private $intabulare;

    /**
     * @var int
     *
     * @ORM\Column(name="grupuri_sanitare", type="smallint",nullable=true)
     * * @Assert\Regex(
     *     pattern="/\d/",
     *     message="Acest camp contine numai cifre"
     * )
     * @Assert\Regex(
     *     pattern="/\D/",
     *      match=false,
     *     message="Acest camp contine numai cifre"
     * )
     */
    private $grupuriSanitare;


    public function __construct() {
        $this->matchVA = new ArrayCollection();
    }



    public function getMatchVA()
    {
        return $this->matchVA;
    }

    public function addMatchVA(MatchVA $matchVA){
        $this->matchVA[] = $matchVA;
    }

    public function removeMatchVA(MatchVA $matchVA){
        return $this->matchVA->removeElement($matchVA);
    }


    /**
     * @return int
     */
    public function getAn()
    {
        return $this->an;
    }

    /**
     * @param int $an
     */
    public function setAn($an)
    {
        $this->an = $an;
    }



    /**
     * @return mixed
     */
    public function getTipImobil()
    {
        return $this->tipImobil;
    }

    /**
     * @param mixed $tipImobil
     */
    public function setTipImobil($tipImobil)
    {
        $this->tipImobil = $tipImobil;
    }



    /**
     * @return mixed
     */
    public function getComisionZero()
    {
        return $this->comisionZero;
    }

    /**
     * @param mixed $comisionZero
     */
    public function setComisionZero($comisionZero)
    {
        $this->comisionZero = $comisionZero;
    }



    /**
     * @return mixed
     */
    public function getConfort()
    {
        return $this->confort;
    }

    /**
     * @param mixed NomenclatorType
     */
    public function setConfort(NomenclatorImobile $confort)
    {
        $this->confort = $confort;
    }

    /**
     * @return mixed
     */
    public function getEtaj()
    {
        return $this->etaj;
    }

    /**
     * @param mixed $etaj
     */
    public function setEtaj($etaj)
    {
        $this->etaj = $etaj;
    }




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contact
     *
     * @param \AppBundle\Entity\Contact $contact
     *
     * @return OfertaApartament
     */
    public function setContact(\AppBundle\Entity\Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \AppBundle\Entity\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set camere
     *
     * @param integer $camere
     *
     * @return OfertaApartament
     */
    public function setCamere($camere)
    {
        $this->camere = $camere;

        return $this;
    }

    /**
     * Get camere
     *
     * @return int
     */
    public function getCamere()
    {
        return $this->camere;
    }

    /**
     * Set suprafataUtila
     *
     * @param integer $suprafataUtila
     *
     * @return OfertaApartament
     */
    public function setSuprafataUtila($suprafataUtila)
    {
        $this->suprafataUtila = $suprafataUtila;

        return $this;
    }

    /**
     * Get suprafataUtila
     *
     * @return int
     */
    public function getSuprafataUtila()
    {
        return $this->suprafataUtila;
    }

    /**
     * @return mixed
     */
    public function getCompartimentare()
    {
        return $this->compartimentare;
    }

    /**
     * @param mixed $compartimentare
     */
    public function setCompartimentare(NomenclatorImobile $compartimentare)
    {
        $this->compartimentare = $compartimentare;
    }



    /**
     * Set tipCompartimentare
     *
     * @param \AppBundle\Entity\NomenclatorImobile $tipCompartimentare
     *
     * @return OfertaApartament
     */
    public function setTipCompartimentare(\AppBundle\Entity\NomenclatorImobile $tipCompartimentare = null)
    {
        $this->tipCompartimentare = $tipCompartimentare;

        return $this;
    }

    /**
     * Get tipCompartimentare
     *
     * @return \AppBundle\Entity\NomenclatorImobile
     */
    public function getTipCompartimentare()
    {
        return $this->tipCompartimentare;
    }

    /**
     * Set tipLocuinta
     *
     * @param \AppBundle\Entity\NomenclatorImobile $tipLocuinta
     *
     * @return OfertaApartament
     */
    public function setTipLocuinta(\AppBundle\Entity\NomenclatorImobile $tipLocuinta = null)
    {
        $this->tipLocuinta = $tipLocuinta;

        return $this;
    }

    /**
     * Get tipLocuinta
     *
     * @return \AppBundle\Entity\NomenclatorImobile
     */
    public function getTipLocuinta()
    {
        return $this->tipLocuinta;
    }

    /**
     * Set stadiuConstructie
     *
     * @param \AppBundle\Entity\NomenclatorImobile $stadiuConstructie
     *
     * @return OfertaApartament
     */
    public function setStadiuConstructie(\AppBundle\Entity\NomenclatorImobile $stadiuConstructie = null)
    {
        $this->stadiuConstructie = $stadiuConstructie;

        return $this;
    }

    /**
     * Get stadiuConstructie
     *
     * @return \AppBundle\Entity\NomenclatorImobile
     */
    public function getStadiuConstructie()
    {
        return $this->stadiuConstructie;
    }


    /**
     * Set monedaVanzare
     *
     * @param \AppBundle\Entity\NomenclatorImobile $monedaVanzare
     *
     * @return OfertaApartament
     */
    public function setMonedaVanzare(\AppBundle\Entity\NomenclatorImobile $monedaVanzare = null)
    {
        $this->monedaVanzare = $monedaVanzare;

        return $this;
    }

    /**
     * Get monedaVanzare
     *
     * @return \AppBundle\Entity\NomenclatorImobile
     */
    public function getMonedaVanzare()
    {
        return $this->monedaVanzare;
    }

    /**
     * Add structuraRezistentum
     *
     * @param \AppBundle\Entity\NomenclatorImobile $structuraRezistentum
     *
     * @return OfertaApartament
     */
    public function addStructuraRezistentum(\AppBundle\Entity\NomenclatorImobile $structuraRezistentum)
    {
        $this->structuraRezistenta[] = $structuraRezistentum;

        return $this;
    }

    /**
     * Remove structuraRezistentum
     *
     * @param \AppBundle\Entity\NomenclatorImobile $structuraRezistentum
     */
    public function removeStructuraRezistentum(\AppBundle\Entity\NomenclatorImobile $structuraRezistentum)
    {
        $this->structuraRezistenta->removeElement($structuraRezistentum);
    }

    /**
     * Get structuraRezistenta
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStructuraRezistenta()
    {
        return $this->structuraRezistenta;
    }

    /**
     * Set agentie
     *
     * @param \AppBundle\Entity\Agentie $agentie
     *
     * @return OfertaApartament
     */
    public function setAgentie(\AppBundle\Entity\Agentie $agentie = null)
    {
        $this->agentie = $agentie;

        return $this;
    }

    /**
     * Get agentie
     *
     * @return \AppBundle\Entity\Agentie
     */
    public function getAgentie()
    {
        return $this->agentie;
    }



    /**
     * Set contact
     *
     * @param User $agent
     *
     * @return OfertaApartament
     */
    public function setAgent(User $agent = null)
    {
        $this->agent = $agent;
        return $this;
    }

    /**
     * Get agent
     *
     * @return User
     */
    public function getAgent()
    {
        return $this->agent;
    }


    /**
     * @return int
     */
    public function getSuprafataConstruita()
    {
        return $this->suprafataConstruita;
    }

    /**
     * @param int $suprafataConstruita
     */
    public function setSuprafataConstruita($suprafataConstruita)
    {
        $this->suprafataConstruita = $suprafataConstruita;
    }

    /**
     * @return int
     */
    public function getSuprafataBalconTerasa()
    {
        return $this->suprafataBalconTerasa;
    }

    /**
     * @param int $suprafataBalconTerasa
     */
    public function setSuprafataBalconTerasa($suprafataBalconTerasa)
    {
        $this->suprafataBalconTerasa = $suprafataBalconTerasa;
    }

    /**
     * @return boolean
     */
    public function isCadastru()
    {
        return $this->cadastru;
    }

    /**
     * @param boolean $cadastru
     */
    public function setCadastru($cadastru)
    {
        $this->cadastru = $cadastru;
    }

    /**
     * @return boolean
     */
    public function isIntabulare()
    {
        return $this->intabulare;
    }

    /**
     * @param boolean $intabulare
     */
    public function setIntabulare($intabulare)
    {
        $this->intabulare = $intabulare;
    }

    /**
     * @return int
     */
    public function getGrupuriSanitare()
    {
        return $this->grupuriSanitare;
    }

    /**
     * @param int $grupuriSanitare
     */
    public function setGrupuriSanitare($grupuriSanitare)
    {
        $this->grupuriSanitare = $grupuriSanitare;
    }

    /**
     * @return int
     */
    public function getPret()
    {
        return $this->pret;
    }

    /**
     * @param int $pret
     */
    public function setPret($pret)
    {
        $this->pret = $pret;
    }

    /**
     * @return int
     */
    public function getComisionProcent()
    {
        return $this->comisionProcent;
    }

    /**
     * @param int $comisionProcent
     */
    public function setComisionProcent($comisionProcent)
    {
        $this->comisionProcent = $comisionProcent;
    }

    /**
     * @return int
     */
    public function getComisionSuma()
    {
        return $this->comisionSuma;
    }

    /**
     * @param int $comisionSuma
     */
    public function setComisionSuma($comisionSuma)
    {
        $this->comisionSuma = $comisionSuma;
    }

    /**
     * @return string
     */
    public function getCertificatEnergetic()
    {
        return $this->certificatEnergetic;
    }

    /**
     * @param string $certificatEnergetic
     */
    public function setCertificatEnergetic($certificatEnergetic)
    {
        $this->certificatEnergetic = $certificatEnergetic;
    }

    /**
     * @return string
     */
    public function getReper()
    {
        return $this->reper;
    }

    /**
     * @param string $reper
     */
    public function setReper($reper)
    {
        $this->reper = $reper;
    }

    /**
     * Set gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     *
     * @return OfertaApartament
     */
    public function setGallery(Gallery $gallery = null)
    {
        $gallery->setOfertaApartament($this);
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \AppBundle\Entity\Gallery
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @return string
     */
    public function getDescriereEmotionala()
    {
        return $this->descriereEmotionala;
    }

    /**
     * @param string $descriereEmotionala
     */
    public function setDescriereEmotionala($descriereEmotionala)
    {
        $this->descriereEmotionala = $descriereEmotionala;
    }

    /**
     * @return string
     */
    public function getDescrierePersonala()
    {
        return $this->descrierePersonala;
    }

    /**
     * @param string $descrierePersonala
     */
    public function setDescrierePersonala($descrierePersonala)
    {
        $this->descrierePersonala = $descrierePersonala;
    }

    /**
     * @return string
     */
    public function getAdresa1()
    {
        return $this->adresa1;
    }

    /**
     * @param string $adresa1
     */
    public function setAdresa1($adresa1)
    {
        $this->adresa1 = $adresa1;
    }

    /**
     * @return string
     */
    public function getAdresa2()
    {
        return $this->adresa2;
    }

    /**
     * @param string $adresa2
     */
    public function setAdresa2($adresa2)
    {
        $this->adresa2 = $adresa2;
    }

    /**
     * @return mixed
     */
    public function getMapLat()
    {
        return $this->mapLat;
    }

    /**
     * @param mixed $mapLat
     */
    public function setMapLat($mapLat)
    {
        $this->mapLat = $mapLat;
    }

    /**
     * @return mixed
     */
    public function getMapLng()
    {
        return $this->mapLng;
    }

    /**
     * @param mixed $mapLng
     */
    public function setMapLng($mapLng)
    {
        $this->mapLng = $mapLng;
    }



}
