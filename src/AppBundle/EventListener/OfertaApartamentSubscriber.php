<?php
namespace AppBundle\EventListener;

use AppBundle\Entity\MatchVA;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
// for Doctrine 2.4: Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use AppBundle\Entity\OfertaApartament;

class OfertaApartamentSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate',
        );
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof OfertaApartament) {

            $entityManager = $args->getEntityManager();

            // add Cereri
            $cereri_compatibile = $entityManager->getRepository('AppBundle:CerereApartament')->findCereri($entity);

            if($cereri_compatibile) {
                foreach ($cereri_compatibile as $i => $cerere) {
                    $matchVA = new MatchVA();
                    $matchVA->setOfertaApartament($entity);
                    $matchVA->setCerereApartament($cerere);
                    $entityManager->persist($matchVA);
                    if ($i % 25 == 0) {
                        $entityManager->flush();
                        $entityManager->clear();
                    }
                }
                $entityManager->flush();
                $entityManager->clear();
            }

        }
    }
}

