<?php
/**
 * Created by PhpStorm.
 * User: florin
 * Date: 10/6/16
 * Time: 3:56 PM
 */

namespace AppBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class AbstractModel
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inserted_at", type="datetime", nullable=true)
     */
    protected $insertedAt;


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function insertedAt()
    {

        $this->insertedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getInsertedAt()
    {
        return $this->insertedAt;
    }



}