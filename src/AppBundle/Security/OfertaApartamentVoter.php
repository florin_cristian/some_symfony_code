<?php
/**
 * Created by PhpStorm.
 * User: florin
 * Date: 2/20/17
 * Time: 3:06 PM
 */

namespace AppBundle\Security;


use AppBundle\Entity\OfertaApartament;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class OfertaApartamentVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const CREATE = 'create';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::EDIT, self::CREATE))) {
            return false;
        }

        // only vote on OfertaApartament objects inside this voter
        if (!$subject instanceof OfertaApartament) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        $ofertaApartament = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($ofertaApartament, $user, $token);
            case self::EDIT:
                return $this->canEdit($ofertaApartament, $user, $token);
            case self::CREATE:
                return $this->decisionManager->decide($token, array(User::ROLE_AGENT));
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(OfertaApartament $ofertaApartament, User $user, TokenInterface $token)
    {
        // if they can edit, they can view
        if ($this->canEdit($ofertaApartament, $user, $token)) {
            return true;
        }
    }

    private function canEdit(OfertaApartament $ofertaApartament, User $user, TokenInterface $token)
    {
        if($user === $ofertaApartament->getAgent() || $this->decisionManager->decide($token, array(User::ROLE_SECRETARIAT))) return true;
        else
        return false;
    }
}