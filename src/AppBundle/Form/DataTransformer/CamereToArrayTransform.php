<?php
/**
 * Created by PhpStorm.
 * User: florin
 * Date: 2/3/17
 * Time: 9:56 AM
 */

namespace AppBundle\Form\DataTransformer;


use AppBundle\Entity\NomenclatorZone;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CamereToArrayTransform implements DataTransformerInterface
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }


    public function transform($camere)
    {
        if (null === $camere) {
            return '';
        }

        return str_split($camere);
    }


    public function reverseTransform($camere = null)
    {
        if (!$camere) {
            return;
        }
        return implode(null, $camere);
    }
}