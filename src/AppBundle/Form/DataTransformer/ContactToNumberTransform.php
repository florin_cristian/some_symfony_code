<?php
/**
 * Created by PhpStorm.
 * User: florin
 * Date: 2/3/17
 * Time: 9:56 AM
 */

namespace AppBundle\Form\DataTransformer;


use AppBundle\Entity\Contact;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ContactToNumberTransform implements DataTransformerInterface
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Transforms an object (contact) to a string (number).
     *
     * @param  Contact|null $contact
     * @return string
     */
    public function transform($contact)
    {
        if (null === $contact) {
            return '';
        }

        return $contact->getId();
    }

    /**
     * Transforms a string (number) to an object (Contact).
     *
     * @param  string $contactId
     * @return Contact|null
     * @throws TransformationFailedException if object (Contact) is not found.
     */
    public function reverseTransform($contactId)
    {

        if (!$contactId) {
            return;
        }
        $contact = $this->manager->getRepository('AppBundle:Contact')->find($contactId);

        if (null === $contact) {
            throw new TransformationFailedException(sprintf(
                'Contact cu id "%s" nu exista!',
                $contactId
            ));
        }

        return $contact;
    }
}