<?php

namespace AppBundle\Form;

use AppBundle\Entity\Contact;
use AppBundle\Entity\NomenclatorImobile;
use AppBundle\Entity\OfertaApartament;
use AppBundle\Form\Type\NomenclatorType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use AppBundle\Form\DataTransformer\ZonaToNumberTransform;
use AppBundle\Form\DataTransformer\GalleryToNumberTransform;
use AppBundle\Form\DataTransformer\ContactToNumberTransform;

class OfertaApartamentType extends AbstractType
{
    private $em;
    protected $user;

    public function __construct(EntityManager $em, TokenStorage $tokenStorage)
    {
        $this->em = $em;
        $this->user = $tokenStorage->getToken()->getUser();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contact', HiddenType::class, array(
                'error_bubbling' => false,
            ))
            ->add('utilitati', NomenclatorType::class, array(
                'class' => 'AppBundle\Entity\NomenclatorImobile',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->where('n.imobil = :imobil')
                        ->andWhere('n.numeCamp = :numeCamp')
                        ->setParameters([
                            'imobil' => 'apartamente',
                            'numeCamp' => 'utilitati'
                        ]);
                },
                'choice_label' => function ($nomenclator) {
                    /** @var NomenclatorImobile $nomenclator */
                    return $nomenclator->getIdValoare() . "|" . $nomenclator->getIdParinte() . "|" . $nomenclator->getDenumireOptiune();
                },
                'multiple' => true,
                'expanded' => true,
            ))
            ->add('finisaje', NomenclatorType::class, array(
                'class' => 'AppBundle\Entity\NomenclatorImobile',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->where('n.imobil = :imobil')
                        ->andWhere('n.numeCamp = :numeCamp')
                        ->setParameters([
                            'imobil' => 'apartamente',
                            'numeCamp' => 'finisaje'
                        ]);
                },
                'choice_label' => function ($nomenclator) {
                    return $nomenclator->getIdValoare() . "|" . $nomenclator->getIdParinte() . "|" . $nomenclator->getDenumireOptiune();
                },
                'multiple' => true,
                'expanded' => true,
            ))
            ->add('camere', HiddenType::class, array(
                'error_bubbling' => false,
            ))
            ->add('zona', HiddenType::class, array(
                'error_bubbling' => false,
            ))
            ->add('gallery', HiddenType::class, array(
                'error_bubbling' => false,
            ))
            ->add('zona_autocomplete', TextType::class, array(
                'attr' => array(
                    'class' => 'ui-autocomplete-input',
                    'placeholder' => 'Cauta zona'
                ),
                'mapped' => false,
                'data' => (!empty($options['data']->getZona()) ? $options['data']->getZona()->getDenumireZona() : ""),
            ))
            ->add('suprafataUtila', null, array(
                'attr' => array('class' => 'input-mini'),
            ))
            ->add('suprafataConstruita', null, array(
                'attr' => array('class' => 'input-mini'),
            ))
            ->add('suprafataBalconTerasa', null, array(
                'attr' => array('class' => 'input-mini'),
            ))
            ->add('etaj', EntityType::class, array(
                'class' => 'AppBundle\Entity\NomenclatorImobile',
                'placeholder' => 'Alege...',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->where('n.imobil = :imobil')
                        ->andWhere('n.numeCamp = :numeCamp')
                        ->setParameters([
                            'imobil' => 'apartamente',
                            'numeCamp' => 'etaj'
                        ])//->orderBy('n.denumireOptiune','ASC')
                        ;
                },
                'choice_label' => 'denumireOptiune'
            ))
            ->add('confort', EntityType::class, array(
                'class' => 'AppBundle\Entity\NomenclatorImobile',
                'placeholder' => 'Alege...',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->where('n.imobil = :imobil')
                        ->andWhere('n.numeCamp = :numeCamp')
                        ->setParameters([
                            'imobil' => 'apartamente',
                            'numeCamp' => 'confort'
                        ])//->orderBy('n.denumireOptiune','ASC')
                        ;
                },
                'choice_label' => 'denumireOptiune'
            ))
            ->add('tipCompartimentare', EntityType::class, array(
                'class' => 'AppBundle\Entity\NomenclatorImobile',
                'placeholder' => 'Alege...',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->where('n.imobil = :imobil')
                        ->andWhere('n.numeCamp = :numeCamp')
                        ->setParameters([
                            'imobil' => 'apartamente',
                            'numeCamp' => 'tipcompartimentare'
                        ])//->orderBy('n.denumireOptiune','ASC')
                        ;
                },
                'choice_label' => 'denumireOptiune'
            ))
            ->add('comisionZero', EntityType::class, array(
                'placeholder' => 'Alege...',
                'class' => 'AppBundle\Entity\NomenclatorImobile',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->where('n.imobil = :imobil')
                        ->andWhere('n.numeCamp = :numeCamp')
                        ->setParameters([
                            'imobil' => 'apartamente',
                            'numeCamp' => 'comisionzero'
                        ])//->orderBy('n.denumireOptiune','ASC')
                        ;
                },
                'choice_label' => 'denumireOptiune'
            ))
            ->add('cadastru', ChoiceType::class, array(
                'placeholder' => 'Alege...',
                'choices' => array(
                    'DA' => 1,
                    'NU' => 0,
                )
            ))
            ->add('intabulare', ChoiceType::class, array(
                'placeholder' => 'Alege...',
                'choices' => array(
                    'DA' => 1,
                    'NU' => 0,
                )
            ))
            ->add('grupuriSanitare', null, array(
                'attr' => array('class' => 'input-mini'),
            ))
            ->add('agent', EntityType::class, array(
                'placeholder' => 'Alege...',
                'class' => 'AppBundle\Entity\User',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->where('n.sediu = :sediu')
                        ->setParameter('sediu', $this->user->getSediu());
                },
                'data' => $this->em->getReference('AppBundle:User', $this->user->getId()),
                'choice_label' => 'nume',
            ))
            ->add('reper')
            ->add('comisionProcent', null, array(
                'attr' => array('class' => 'input-mini', 'placeholder' => '%'),
            ))
            ->add('comisionSuma', null, array(
                'attr' => array('class' => 'input-small', 'placeholder' => 'suma fixa'),
            ))
            ->add('pret')
            ->add('certificatEnergetic', ChoiceType::class, array(
                'placeholder' => 'Alege...',
                'choices' => array_flip(range('A', 'H')),

            ))
            ->add('descriereEmotionala', null, array(
                'attr' => array('class' => 'form-control tinymce'),
            ))
            ->add('descrierePersonala', null, array(
                'attr' => array('class' => 'form-control tinymce'),
            ))
            ->add('adresa1', null, array(
                'attr' => array('placeholder' => 'Strada nr'),
            ))
            ->add('adresa2', null, array(
                'attr' => array('placeholder' => 'Bloc, scara, interfon ... etc'),
            ))
            ->add('mapLat', HiddenType::class, array(
                'error_bubbling' => false,
            ))
            ->add('mapLng', HiddenType::class, array(
                'error_bubbling' => false,
            ));

        $builder->get('zona')->addModelTransformer(new ZonaToNumberTransform($this->em));
        $builder->get('gallery')->addModelTransformer(new GalleryToNumberTransform($this->em));
        $builder->get('contact')->addModelTransformer(new ContactToNumberTransform($this->em));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\OfertaApartament',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_ofertaapartament';
    }


}
